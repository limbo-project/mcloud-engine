#!/bin/bash

echo "Starting the container ..."
docker-compose -f docker-compose-mcloud.yml up -d
echo "Wait 5 minutes ..."
sleep 300

echo "Now check every minute if workers are done"
count=$(docker-compose  -f docker-compose-mcloud.yml ps | grep worker | grep Up | wc -l | tr -d '[:space:]')
i=0
while [ $count -gt $i ]
    do
        sleep 60
        count=$(docker-compose  -f docker-compose-mcloud.yml ps | grep worker | grep Up | wc -l | tr -d '[:space:]')
        echo "Check again ... $count"
    done

echo "Stop the container ..."
docker-compose -f docker-compose-mcloud.yml stop

echo "Combine results to one file ..."
mkdir data/output
mkdir data/final
java -jar ../Dateien/target/mCloud-DCAT-Engine-1.0.4.jar -i ./data/worker1 -s false -o ./data/output
java -jar ../Dateien/target/mCloud-DCAT-Engine-1.0.4.jar -i ./data/worker2 -s false -o ./data/output
java -jar ../Dateien/target/mCloud-DCAT-Engine-1.0.4.jar -i ./data/worker3 -s false -o ./data/output
java -jar ../Dateien/target/mCloud-DCAT-Engine-1.0.4.jar -i ./data/output -s false -o ./data/final
echo "Done. See ./data/final"
