package org.aksw.limbo.mwbt.util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.aksw.limbo.mwbt.util.validators.InputFilesValidator;
import org.aksw.limbo.mwbt.util.validators.OutputFilesValidator;
import org.apache.jena.riot.Lang;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.Parameter;


/**
 * @author ivierle
 * Class to define and validate the entrypoints input parameters using the JCommander library {@link http://jcommander.org/}
 */
public class ParameterHelper
{
    private static final Logger log = LoggerFactory.getLogger(ParameterHelper.class);

    /**
     * Default name base for DCAT catalog files, appended by creation TimeStamp
     */
    private static final String catalogFileName = "dcat-mCloud-metadata";
    /**
     * Default directory to put DCAT catalogs into
     */
    private static final String defaultOutputDirName = "catalogs";

    @Parameter(
        names = {"-i", "--input"},
        description = "*required, path/to/input/directory containing mCloud metadata files in DCAT format, i.e. the output of the SQUIRREL scraping process",
        required = true,
        validateWith = InputFilesValidator.class)
    private String inputDirectoryPath;

    @Parameter(
        names = {"-s", "--store"},
        description = "*required, boolean which determines whether to persist the current runs changes to the triple store."
            + " If false, the current changes are stored in a .ttl file but do not affect the TDB model",
        arity = 1,
        required = true)
    private boolean storeToTDB;

    @Parameter(
        names = {"-l", "--location"},
        description = "[optional], path/to/tdb-assembler-file provide a TDB assembler file to configure the triple store created by Jena. "
            + "The location of the store has to be specified in the assembler file through parameter tdb:location. Make sure the required resources exist. "
            + "Can only be used in conjunction with store=true. "
            + "If store is true but no additional assembler file is specified, the TDB creation will fall back to TDB/tdb-assembler.ttl ",
        required = false,
        validateWith = OutputFilesValidator.class)
    private String tdbLocation;

    @Parameter(
        names = {"-o", "--output"},
        description = "[optional], path/to/output/directory determines where to store the dcat catalog as .ttl file."
            + " If not set a new ouput subdirectory will be created in the input directory.",
        required = false,
        validateWith = OutputFilesValidator.class)
    private String outputDirectoryPath = null;

    @Parameter(
        names = {"-a", "--append"},
        description = "[optional], path/to/directory containing old dcat catalgos as TURTLE files which shall be updated.",
        required = false,
        validateWith = InputFilesValidator.class)
    private String dcatDirectoryPath = null;

    @Parameter(names = {"-h", "--help"}, description = "Print this help message.", help = true)
    private boolean help;

    /**
     * Creates the output file for the DCAT catalog. 
     * The fileName consists of {@link ParameterHelper#catalogFileName} appended by the creation TimeStamp and the TURTLE file extension. 
     * If no outputDirectory path is given via option -o, the file is created in a new subDirectory {@link ParameterHelper#defaultOutputDirName} 
     * that is created inside the inputDirectory
     * @return the file to store the catalog into
     * @throws IOException if handling or creation of files and directories fails
     */
    public File validateAndGetOutputFile() throws IOException
    {
        Date timeStampDate = new Date(System.currentTimeMillis());
        SimpleDateFormat dt = new SimpleDateFormat("[yyyy-MM-dd_HH:mm:ss]");
        String dateString = dt.format(timeStampDate).toString();
        String fileExtension = Lang.TTL.getFileExtensions().get(0);
        String dcatFileName = catalogFileName + "-" + dateString + "." + fileExtension;

        if (outputDirectoryPath == null)
        {
            outputDirectoryPath = inputDirectoryPath + File.separator + defaultOutputDirName;
            File directory = new File(outputDirectoryPath);
            if (!directory.exists())
            {
                if (directory.mkdir())
                {
                    log.info("Created default output directory in input directory.");
                }
                else
                {
                    log.error("Error creating default output directory at " + outputDirectoryPath);
                }
            }
        }

        File file = new File(outputDirectoryPath + File.separator + dcatFileName);
        if (!file.exists())
        {
            if (file.createNewFile())
            {
                log.info("Created new output file for the dcat catalog at " + file.getAbsolutePath());
            }
            else
            {
                log.error("Error creating catalog output file at " + file.getAbsolutePath());
                System.exit(1);
            }
        }
        return file;
    }

    ////
    //Getters
    ////

    public String getInputDirectoryPath()
    {
        return inputDirectoryPath;
    }

    public File getInputDirectory()
    {
        return new File(inputDirectoryPath);
    }

    public String getOutputDirectoryPath()
    {
        return outputDirectoryPath;
    }

    public String getDcatDirectoryPath()
    {
        return dcatDirectoryPath;
    }

    public File getDcatDirectory()
    {
        return dcatDirectoryPath != null ? new File(dcatDirectoryPath) : null;
    }

    public boolean isStoreToTDB()
    {
        return storeToTDB;
    }

    public boolean isHelp()
    {
        return help;
    }

    public File getTDBAssemblerFile()
    {
        return tdbLocation != null ? new File(tdbLocation) : null;
    }
}
