package org.aksw.limbo.mwbt.dcat;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.aksw.limbo.mwbt.util.jena.JenaFileIOHandler;
import org.aksw.limbo.mwbt.util.jena.TDBConnection;
import org.aksw.limbo.mwbt.util.vocabulary.Vocab;
import org.apache.jena.ext.com.google.common.collect.Iterators;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.NodeIterator;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.XSD;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author ivierle
 * Class responsible for creating and updating DCAT catalogs
 */
public class DcatConverter
{
    private static final Logger log = LoggerFactory.getLogger(DcatConverter.class);

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

    private JenaFileIOHandler fileHandler;
    private TDBConnection tdbHandler;
    private boolean storeToTDB;

    /**
     * Constructs a new DcatConverter with file and database handlers
     * @param storeToTDB true if additions or changes shall be persisted to the underlying TDB triple store
     * @param tdbLocation optional parameter providing the path to an user provided tdb assembler file
     */
    public DcatConverter(boolean storeToTDB, File tdbLocation)
    {
        fileHandler = new JenaFileIOHandler();
        this.storeToTDB = storeToTDB;

        if (storeToTDB)
        {
            tdbHandler = tdbLocation == null ? new TDBConnection() : new TDBConnection(tdbLocation);
        }
    }

    /**
     * Triggers the creation of a DCAT catalog 
     * @param metadataInputDirectory directory containing SQUIRREL output files with MetaData scraped from mCloud (no nested directories please)
     * @param outputFile file to store the created catalog to, language is TURTLE
     * @param dcatInputDirectory (optional) path to a directory containing DCAT catalogs that shall be appended or updated 
     * if {@link DcatConverter#storeToTDB} is true the updates are persisted to the triple store
     */
    public void triggerCatalogCreation(File metadataInputDirectory, File outputFile, File dcatInputDirectory)
    {
        Model existingDcatFromFile = null;
        Model squirrelDataModel = null;
        Model catalogModel = null;

        log.debug("Reading mCloud metadata files from " + metadataInputDirectory.getAbsolutePath());
        squirrelDataModel = fileHandler.readRDFFilesIntoModel(metadataInputDirectory.listFiles());

        if (dcatInputDirectory != null)
        {
            log.debug("Reading existing catalog from " + dcatInputDirectory.getAbsolutePath());
            existingDcatFromFile = fileHandler.readRDFFilesIntoModel(dcatInputDirectory.listFiles());
        }

        //---- DEBUG ----
        //fileHandler.writeModelToFile(squirrelDataModel, new File("/home/ivierle/Documents/Uni/7.Semester/BachelorThesis/SquirrelScraped-Data/testmetadata/dcat/debugModel/mergedModelDebug.ttl"),
        //Lang.TURTLE);
        //----

        try
        {
            catalogModel = createCatalog(squirrelDataModel, existingDcatFromFile);
            fileHandler.writeModelToFile(catalogModel, outputFile, Lang.TTL);

            if (storeToTDB)
            {
                log.debug("Persisting current changes to the TDB triple store.");
                List<Statement> stmtsToAdd = getStatements(catalogModel.listStatements());
                tdbHandler.addStatements(null, stmtsToAdd);

                tdbHandler.endTransactionCommit();
                tdbHandler.close();
            }
        }
        catch (RuntimeException e)
        {
            if (storeToTDB)
            {
                tdbHandler.endTransactionAbort();
                tdbHandler.close();
            }
            throw e;
        }
    }

    /**
     * Creates the DCAT catalog including the publisher (LIMBO organization) and fills it with data 
     * @param squirrelDataModel the graph with all the separate MetaData (DataSets and Distributions) provided by SQUIRREL 
     * that will be merged into one coherent catalog
     * @param dcatFromFile (optional) existing catalog to be appended or updated
     * @return the catalog graph as a JENA Model
     * if {@link DcatConverter#storeToTDB} is true the updates are persisted to the triple store
     */
    private Model createCatalog(Model squirrelDataModel, Model dcatFromFile)
    {
        //merge existing catalogs according to input
        Model existingCatalog = null;
        if (storeToTDB)
        {
            log.info("Opening Transaction, current changes will be persisted to the triple store");
            tdbHandler.openTransactionReadWrite();

            Model existingTDB = tdbHandler.getTDBModel(null);
            existingCatalog = dcatFromFile != null ? ModelFactory.createUnion(existingTDB, dcatFromFile) : existingTDB;
            log.debug(Iterators.size(existingCatalog.listResourcesWithProperty(Vocab.type, Vocab.Dataset))
                + " Datasets have been found in the catalog and will be updated if necessary.");
        }
        else
        {
            if (dcatFromFile != null)
            {
                existingCatalog = dcatFromFile;
                log.debug(Iterators.size(existingCatalog.listResourcesWithProperty(Vocab.type, Vocab.Dataset))
                    + " Datasets have been found in the catalog and will be updated if necessary.");
            }
            else
            {
                log.debug("A new catalog will be created.");

            }
        }

        //create Catalog
        Model baseModel = ModelFactory.createDefaultModel();
        Vocab.setPrefixes(baseModel);

        Resource catalog = null;
        String currentTime = LocalDateTime.now().format(formatter);
        if (existingCatalog != null && existingCatalog.contains(null, Vocab.type, Vocab.catalog))
        {
            ResIterator catalogIt = existingCatalog.listSubjectsWithProperty(Vocab.type, Vocab.catalog);
            while (catalogIt.hasNext())
            {
                catalog = catalogIt.next();
                log.debug("Catalog " + catalog + " will be updated");
                catalog.addProperty(Vocab.modified, existingCatalog.createTypedLiteral(currentTime, XSD.dateTime.getURI()));
            }
        }
        if (catalog == null)
        {
            //create LIMBO Agent
            Resource catalogPublisher = baseModel.createResource(Vocab.mcloud + "LimboConsortium");
            catalogPublisher.addProperty(Vocab.type, Vocab.organization);
            catalogPublisher.addProperty(Vocab.label, "The Organization publishing the Dcat metadata catalog about mCloud resources");

            //create catalog info 
            catalog = baseModel.createResource(Vocab.mcloud + "Catalog");
            catalog.addProperty(Vocab.type, Vocab.catalog);
            catalog.addProperty(Vocab.title, "Catalog about resources collected by mCloud");
            catalog.addProperty(Vocab.homepage, "https://www.limbo-project.org/");
            catalog.addProperty(Vocab.publisher, catalogPublisher);
            catalog.addProperty(Vocab.language, "http://id.loc.gov/vocabulary/iso639-1/de");
            catalog.addProperty(Vocab.issued, baseModel.createTypedLiteral(currentTime, XSD.dateTime.getURI()));
        }

        Model dcatModel = existingCatalog != null ? ModelFactory.createUnion(existingCatalog, baseModel) : baseModel;

        //get data from merged SQUIRREL graph
        ResIterator squirrelDatasets = squirrelDataModel.listResourcesWithProperty(Vocab.type, Vocab.Dataset);

        while (squirrelDatasets.hasNext())
        {
            Resource dataSet = squirrelDatasets.next();
            String dataSetURI = dataSet.getURI();

            if (dcatModel.containsResource(ResourceFactory.createResource(dataSetURI)))
            {
                Resource dataSetToUpdate = dcatModel.getResource(dataSetURI);
                Resource updatedDataSet = updateDataSetAndDistributions(dcatModel, dataSet, dataSetToUpdate);
                Resource catalogEntry = copyResourceToCatalog(dcatModel, updatedDataSet);
                catalog.addProperty(Vocab.dataset, catalogEntry);
            }
            else
            {
                Resource catalogEntry = copyResourceToCatalog(dcatModel, dataSet);
                catalog.addProperty(Vocab.dataset, catalogEntry);
            }
        }

        return dcatModel;
    }

    ////
    //Comparison and update of DataSets and Distributions
    ////

    /**
     * Updates resources that already exist in the catalog (if necessary) in several steps:
     * - checks if the Distributions of the DataSet had any actual value changes 
     * - updates solely the Distribution values and TimeStamps accordingly
     * - checks if the DataSet had any actual value changes or additions 
     * - updates the DataSet accordingly and adds the (possibly updated) Distributions
     * When checking for changes in resources only values that are not TimeStamps (i.e. issued, modified) are considered,
     * a resource that has identical values with differing TimeStamps is considered to be the same and will not be updated.
     * For both operations the checks include all possibly nested resources (e.g. License or Provider) and registers these changes.
     * @param catalogModel the catalog graph to store the updated resources to
     * @param newDataSet the DataSet (possibly) containing new values
     * @param dataSetToUpdate the old DataSet that (possibly) needs to be updated
     * @return the DataSet resource, if no changes have been registered the old DataSet is returned
     */
    private Resource updateDataSetAndDistributions(Model catalogModel, Resource newDataSet, Resource dataSetToUpdate)
    {
        //keep track of the newest modification
        Set<Literal> modificationTimestamps = new HashSet<>(getObjectsAsLiterals(catalogModel.listObjectsOfProperty(dataSetToUpdate, Vocab.modified)));

        //keep track of distributions to remove and have fun with stream API        
        List<Resource> distributionLibrary =
            getStatements(dataSetToUpdate.listProperties(Vocab.distribution)).stream().filter(s -> s.getObject().isResource()).collect(Collectors.toList()).stream().map(s -> s.getResource())
                .collect(Collectors.toList());

        //the DataSet
        Resource dataSet = dataSetToUpdate;

        //check if Distributions of the DataSet changed and update if applicable
        Map<Resource, Boolean> distributionsMap = compareDataSetDistributions(newDataSet, dataSetToUpdate);
        Iterator<Resource> distributionsIter = distributionsMap.keySet().iterator();
        while (distributionsIter.hasNext())
        {
            Resource distribution = distributionsIter.next();
            if (!distributionsMap.get(distribution))
            {
                if (catalogModel.containsResource(ResourceFactory.createResource(distribution.getURI())))
                {
                    log.debug("Updating existing distribution " + distribution.getURI());

                    Resource distributionToUpdate = catalogModel.getResource(distribution.getURI());

                    //TimeStamp the distribution was issued in existing catalog
                    Literal issued = distributionToUpdate.getProperty(Vocab.issued).getLiteral();
                    //TimeStamps the distribution was modified in existing catalog 
                    List<Literal> modifications = getObjectsAsLiterals(catalogModel.listObjectsOfProperty(distributionToUpdate, Vocab.modified));
                    //add current modification timeStamp of distribution 
                    modifications.add(distribution.getProperty(Vocab.issued).getLiteral());
                    //Time Points as period of time with temporal from existing catalog
                    List<Literal> temporals = getObjectsAsLiterals(catalogModel.listObjectsOfProperty(distributionToUpdate, Vocab.temporal));

                    //remove distribution issued which represents the new modification time to replace with old one 
                    distribution.removeAll(Vocab.issued);
                    //remove old distribution and nested values from catalog to replace with new ones
                    removeDeprecatedValues(catalogModel, distribution);

                    //get newest modification time from list
                    modificationTimestamps.add(getNewestLiteral(new HashSet<>(modifications)));

                    //add time related properties to new distribution and store
                    addLiteralValues(distribution, Vocab.issued, Arrays.asList(issued));
                    addLiteralValues(distribution, Vocab.modified, modifications);
                    addLiteralValues(distribution, Vocab.temporal, temporals);

                }
                else
                {
                    modificationTimestamps.add(distribution.getProperty(Vocab.issued).getLiteral());
                }
                //add updated distribution to dataSet
                newDataSet.addProperty(Vocab.distribution, distribution);

                //update dataSet
                dataSet = newDataSet;
            }
            else
            {
                //add unchanged distribution to (updated) dataset
                Resource dist = distributionLibrary.stream().filter(d -> d.getURI().equals(distribution.getURI())).findFirst().orElse(null);
                if (dist != null)
                {
                    newDataSet.addProperty(Vocab.distribution, dist);
                }
            }
            distributionLibrary.remove(catalogModel.getResource(distribution.getURI()));
        }

        //update the datasets modification timestamp if
        //1) the dataset changed
        //2) a distribution of the dataset changed
        //3) a distribution was removed from the dataset
        //if more than one apply the newest modification timestamp is chosen
        if (!compareResources(newDataSet, dataSetToUpdate, true) || !modificationTimestamps.isEmpty() || !distributionLibrary.isEmpty())
        {
            log.debug("Updating existing dataset " + dataSetToUpdate.getURI());

            Literal datasetIssued = dataSetToUpdate.getProperty(Vocab.issued).getLiteral();
            modificationTimestamps.add(newDataSet.getProperty(Vocab.issued).getLiteral());

            //remove values for old version of dataset
            removeDeprecatedValues(catalogModel, dataSetToUpdate);
            newDataSet.removeAll(Vocab.issued);

            //adjust timestamps for new version of dataset            
            addLiteralValues(newDataSet, Vocab.issued, Arrays.asList(datasetIssued));
            addLiteralValues(newDataSet, Vocab.modified, new ArrayList<>(modificationTimestamps));

            dataSet = newDataSet;
        }

        //if a distribution was removed for an existing dataset it is removed from the model
        removeDeprecatedResources(catalogModel, distributionLibrary);

        return dataSet;
    }

    /**
     * Compares all Distributions of two given DataSets to determine if any changes in this nested resource occurred. 
     * This needs to be done separately because DataSets and Distributions each store TimeStamps and can be updated independently. 
     * @param dataSet1 the DataSet (possibly) containing new values
     * @param dataSet2 the old DataSet that (possibly) needs to be updated
     * @return a Collection that maps a boolean to each Distribution, true means the Distributions are identical i.e. no changes have been registered 
     */
    private Map<Resource, Boolean> compareDataSetDistributions(Resource dataSet1, Resource dataSet2)
    {
        Map<Resource, Boolean> checkedDistributions = new HashMap<>();

        //compare Distributions
        StmtIterator distributionIter = dataSet1.listProperties(Vocab.distribution);
        while (distributionIter.hasNext())
        {
            Resource distribution1 = distributionIter.next().getResource();

            if (dataSet2.hasProperty(Vocab.distribution, distribution1))
            {
                checkedDistributions.put(distribution1, compareResources(dataSet2, distribution1, false));
            }
            else
            {
                //add new distribution if it did not exist before
                checkedDistributions.put(distribution1, false);
            }
        }

        return checkedDistributions;
    }

    ////
    // Helper methods
    ////

    /**
     * Adds resources associated with the merged Model from SQUIRREL to the created DCAT catalog
     * @param dcatCatalog to add statements to
     * @param resource to add
     * @return the resource now associated with the catalog model
     */
    private Resource copyResourceToCatalog(Model dcatCatalog, Resource resource)
    {
        Resource copy = dcatCatalog.createResource(resource.getURI());
        //weirdly this throws a ConcurrentModificationException if we don't get the list from iterator but ONLY if we persist to TDB
        List<Statement> properties = getStatements(resource.listProperties());
        for (Statement stmt : properties)
        {
            copy.addProperty(stmt.getPredicate(), stmt.getObject());
            getNestedResource(dcatCatalog, stmt.getObject());
        }

        return copy;
    }

    /**
     * Recursively adds all nested resources from the SQUIRREL model to the catalog model
     * @param dcatCatalog to add the nested resources to
     * @param object the resource to add with possible nested resources
     */
    private void getNestedResource(Model dcatCatalog, RDFNode object)
    {
        if (object.isResource())
        {
            Resource nested = (Resource) object;
            if (dcatCatalog.containsResource(ResourceFactory.createResource(nested.getURI())))
            {
                //weirdly this throws a ConcurrentModificationException if we don't get the list from iterator but ONLY if we persist to TDB
                List<Statement> nestedResources = getStatements(object.getModel().listStatements(nested, null, (RDFNode) null));
                for (Statement stmt : nestedResources)
                {
                    dcatCatalog.add(stmt);
                    getNestedResource(dcatCatalog, stmt.getObject());
                }
            }
        }
    }

    /**
     * Compare two resources for equality to determine if an update of the resource in necessary.
     * Two resources are considered equal, if all values (included nested resources) except for the TimeStamps are equal.
     * @param resource1 to compare
     * @param resource2 to compare
     * @param listAll if true all properties of resource1 are listed and compared to resource2, 
     * else only statements from resource1s model having resource2 as a subject are considered
     * @return true if the resources are found to be identical/equal
     */
    private boolean compareResources(Resource resource1, Resource resource2, boolean listAll)
    {
        boolean identical = true;

        StmtIterator resourceIterator =
            listAll ? resource1.listProperties() : resource1.getModel().listStatements(resource2, null, (RDFNode) null);

        while (resourceIterator.hasNext())
        {
            identical = identical && compareNestedResource(resource2, resourceIterator.next());
            if (!identical)
            {
                //escape as soon as Resources where found not to be identical
                break;
            }
        }
        return identical;
    }

    /**
     * Compares statements to statements of the resource, if the statements Object is a resource
     * the nested resources are compared recursively
     * @param resource to compare
     * @param stmt to compare to resources statements
     * @return true if all statements and nested resources are identical/equal
     */
    private boolean compareNestedResource(Resource resource, Statement stmt)
    {
        boolean identical = true;

        if (stmt.getObject().isResource())
        {
            Resource nestedResource = stmt.getResource();
            if (!resource.getModel().containsResource(nestedResource))
            {
                return false;
            }
            else
            {
                StmtIterator nestedStatements = resource.getModel().listStatements(nestedResource, null, (RDFNode) null);
                while (nestedStatements.hasNext())
                {
                    identical = identical && compareNestedResource(nestedResource, nestedStatements.next());
                    if (!identical)
                    {
                        return false;
                    }
                }
            }
        }
        else
        {
            Property predicate = stmt.getPredicate();
            //exclude TimeStamps as they are not to be considered for equality
            if (!predicate.equals(Vocab.issued) && !predicate.equals(Vocab.modified))
            {
                identical = identical && resource.hasProperty(predicate, stmt.getObject());
            }
        }
        return identical;
    }

    /**
     * Removes deprecated values that need to be updated for a given @param resource from the @param model 
     * if {@link DcatConverter#storeToTDB} is true the updates are persisted to the triple store
     */
    private void removeDeprecatedValues(Model model, Resource resource)
    {
        List<Statement> remove = getStatements(model.listStatements(resource, null, (RDFNode) null));
        for (Statement stmt : remove)
        {
            removeNestedResource(model, stmt);
        }
    }

    /**
     * Recursively removes all nested statements (if object is a resource) from the given model
     * @param model the model to remove statements from
     * @param statement the statement to remove with nested resources
     * if {@link DcatConverter#storeToTDB} is true the removal is persisted to the triple store
     */
    private void removeNestedResource(Model model, Statement statement)
    {
        if (statement.getObject().isResource())
        {
            Resource nested = statement.getResource();
            List<Statement> nestedRemove = getStatements(model.listStatements(nested, null, (RDFNode) null));
            if (nestedRemove.isEmpty())
            {
                removeDeprecatedResources(model, Arrays.asList(statement.getSubject()));
            }

            for (Statement nstmt : nestedRemove)
            {
                removeNestedResource(model, nstmt);
            }
        }
        else
        {
            if (storeToTDB)
            {
                tdbHandler.removeStatement(null, statement);
            }
            model.remove(statement);
        }
    }

    /**
     * If a resource is to be completely removed from the model,
     * this method removes all statements where this resource is either the subject or the object of the statement
     * @param model the model to remove statements from
     * @param resourcesToDelete a list of resources to purge from the model
     * if {@link DcatConverter#storeToTDB} is true the removal is persisted to the triple store
     */
    private void removeDeprecatedResources(Model model, List<Resource> resourcesToDelete)
    {
        for (Resource remove : resourcesToDelete)
        {
            if (storeToTDB)
            {
                tdbHandler.removeStatements(null, getStatements(model.listStatements(remove, null, (RDFNode) null)));
                tdbHandler.removeStatements(null, getStatements(model.listStatements(null, null, remove)));
            }
            model.remove(model.listStatements(remove, null, (RDFNode) null));
            model.remove(model.listStatements(null, null, remove));
        }
    }

    /**
     * Creates a list of statements from a @param stmtIter to deal with ConcurrentModificationException
     * @return a list of all statements contained in the iterator
     */
    private List<Statement> getStatements(StmtIterator stmtIter)
    {
        List<Statement> stmtList = new ArrayList<>();
        while (stmtIter.hasNext())
        {
            stmtList.add(stmtIter.next());
        }
        return stmtList;
    }

    /**
     * @param timestamps a set of Literals of type XSD.DateTime to compare to each other
     * @return the literal representing the latest date, i.e. the newest modification
     */
    private Literal getNewestLiteral(Set<Literal> timestamps)
    {
        //this seems rather laborious but i did not find a way to sort typed literals for XSD.DateTime
        HashMap<String, Literal> dateMap = new HashMap<>();
        Iterator<Literal> iterator = timestamps.iterator();
        while (iterator.hasNext())
        {
            Literal current = iterator.next();
            dateMap.put(current.getString(), current);
        }

        List<String> keySet = new ArrayList<>(dateMap.keySet());
        Collections.sort(keySet, (s1, s2) -> LocalDateTime.parse(s1, formatter).compareTo(LocalDateTime.parse(s2, formatter)));

        return dateMap.get(keySet.get(keySet.size() - 1));
    }

    private List<Literal> getObjectsAsLiterals(NodeIterator nodeIter)
    {
        List<Literal> literals = new ArrayList<>();
        while (nodeIter.hasNext())
        {
            RDFNode node = nodeIter.next();
            if (node.isLiteral())
            {
                literals.add(node.asLiteral());
            }
        }

        return literals;
    }

    private void addLiteralValues(Resource subject, Property predicate, List<Literal> values)
    {
        for (Literal value : values)
        {
            subject.addProperty(predicate, value);
        }
    }
}
