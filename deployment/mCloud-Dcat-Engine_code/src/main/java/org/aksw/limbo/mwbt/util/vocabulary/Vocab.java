package org.aksw.limbo.mwbt.util.vocabulary;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.vocabulary.DCAT;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.apache.jena.vocabulary.XSD;


/**
 * Class to collect and combine all used name spaces and properties 
 * which all other classes reference to simplify changing URIs
 */
public class Vocab
{
    //NameSpaces
    public static final String dct = DCTerms.getURI();
    public static final String rdfs = RDFS.getURI();
    public static final String rdf = RDF.getURI();
    public static final String xsd = XSD.getURI();
    public static final String foaf = FOAF.getURI();
    public static final String dcat = DCAT.getURI();
    public static final String mcloud = MCLOUD.getURI();

    public static void setPrefixes(Model model)
    {
        model.setNsPrefix("dcat", dcat);
        model.setNsPrefix("dcterms", dct);
        model.setNsPrefix("foaf", foaf);
        model.setNsPrefix("rdf", rdf);
        model.setNsPrefix("rdfs", rdfs);
        model.setNsPrefix("xsd", xsd);
        model.setNsPrefix("mcloud", mcloud);
    }

    //dcat
    public static final Resource catalog = DCAT.Catalog;
    public static final Resource Dataset = DCAT.Dataset;
    public static final Property dataset = DCAT.dataset;
    public static final Resource Distribution = DCAT.Distribution;
    public static final Property distribution = DCAT.distribution;

    //other 
    public static final Property type = RDF.type;
    public static final Property label = RDFS.label;
    public static final Property title = DCTerms.title;
    public static final Property publisher = DCTerms.publisher;
    public static final Resource organization = FOAF.Organization;
    public static final Property issued = DCTerms.issued;
    public static final Property modified = DCTerms.modified;
    public static final Property language = DCTerms.language;
    public static final Property homepage = FOAF.homepage;
    public static final Property temporal = DCTerms.temporal;
}
