package org.aksw.limbo.mwbt.util.vocabulary;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;


/**
 * Vocabulary defined in the scope of mCloud data retrieval,
 * containing required Properties and Resources that were not sufficiently defined in other vocabularies 
 * LMCSE is an abbreviation for LimboMCloudStatisticsEngine
 */
public class MCLOUD
{
    /** 
     * <p>The RDF model that holds the vocabulary terms</p>
     */
    private static final Model M_MODEL = ModelFactory.createDefaultModel();

    /** 
     * <p>The namespace of the vocabulary as a string</p> 
     */
    public static final String NS = "https://metadata.limbo-project.org/";

    /** 
     * <p>The namespace of the vocabulary as a string</p>
     * @return namespace as String
     * @see #NS 
     */
    public static String getURI()
    {
        return NS;
    }

    /** 
     * <p>The namespace of the vocabulary as a resource</p> 
     */
    public static final Resource NAMESPACE = M_MODEL.createResource(NS);

    /** 
     * <p>The ontology's owl:versionInfo as a string</p> 
     */
    public static final String VERSION_INFO = "1.0";

    /** 
     * <p>indicates which protocol/API/Webservice can be used to download data</p>
     */
    public static final Property accessType = M_MODEL.createProperty(NS + "accessType");

    /** 
     * <p>Placeholder to collect all Datasets that have no license attached or errors retrieving the license</p>
     */
    public static final String NullLicense = NS + "NullLicense";

    /**
     *  <p>Placeholder to collect all Datasets that have no publisher attached or errors retrieving the publisher</p>
     */
    public static final String NullPublisher = NS + "NullPublisher";

    /**
     *  <p>Placeholder to collect all Distributions for which the accessType of the URI could not be parsed</p>
     */
    public static final String UnknownAccessType = NS + "UnknownAccessType";

    /**
     * <p>Base for URI creation of Datasets for DCAT catalog</p>
     */
    public static final String DataSetUriBase = NS + "dataset-";

    /**
     * <p>Base for URI creation of Distributions for DCAT catalog</p>
     */
    public static final String DistributionUriBase = NS + "distribution-";
}
