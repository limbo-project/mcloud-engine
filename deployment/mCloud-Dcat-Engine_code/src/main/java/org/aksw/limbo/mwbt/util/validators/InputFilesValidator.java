package org.aksw.limbo.mwbt.util.validators;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

/**
 * @author ivierle
 * Validatorclass used to validate input parameters
 */
public class InputFilesValidator implements IParameterValidator
{
    private static final Logger log = LoggerFactory.getLogger(InputFilesValidator.class);

    /**
     * {@inheritDoc}
     * Checks the input directories for invalid values.
     * Throws {@link ParameterException} if the input directory does not exist or if it is empty and terminates the program.
     */
    @Override
    public void validate(String name, String value) throws ParameterException
    {
        File resourceDirectory = new File(value);
        if (!resourceDirectory.exists())
        {
            log.error("[ValidationError] The input directory does not exist. Please check if the given path is correct or use --help for instructions.",
                new ParameterException("Parameter " + name + " does not point to a valid directory"));
            System.exit(1);
        }
        else
        {
            File[] resourceList = resourceDirectory.listFiles();
            List<File> plainFiles = new ArrayList<>();
            for (File file : resourceList)
            {
                if (!file.isDirectory())
                {
                    plainFiles.add(file);
                }
            }
            if (plainFiles.size() == 0)
            {
                log.error("[ValidationError] The input directory is empty. Please make sure there is mCloud metadata and/or dcat catalogs in the input folder. Use --help for instructions.",
                    new ParameterException("Parameter " + name + " directory does not contain any files."));
                System.exit(1);
            }
        }
    }
}
