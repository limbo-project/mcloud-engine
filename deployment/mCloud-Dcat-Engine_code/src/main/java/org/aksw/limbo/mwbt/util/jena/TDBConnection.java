package org.aksw.limbo.mwbt.util.jena;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.jena.query.Dataset;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Selector;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.tdb.TDBFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author ivierle
 * Helperclass that handles persisting data to the TDB triple store
 */
public class TDBConnection
{
    private static final Logger log = LoggerFactory.getLogger(TDBConnection.class);

    /**
     * [FALLBACK] required assembler file with definitions for TDB setup
     */
    private static final String TDBAssemblerFile = "TDB/tdb-assembler.ttl";
    /**
     * [FALLBACK] the database path (has to match location in assember file)
     */
    private static final String tdbDataset = "TDB/store/Dcat-DB";
    /**
     * The Dataset containing the metadata graphs to be stored
     */
    private Dataset dataset;

    /**
     * Constructs a new TDB database from assembler file if necessary or opens a connection to the existing triple store
     * As no additional assembler file is provided, the default triple store is created in the application context under
     * TDB/store/Dcat-DB using the assembler file TDB/tdb-assembler.ttl
     */
    public TDBConnection()
    {
        this(new File(TDBAssemblerFile));
        
        //matches the tdb:location parameter in the default assembler file
        File file = new File(tdbDataset);
        if (!file.exists())
        {
            if (!file.getParentFile().exists())
            {
                file.getParentFile().mkdirs();
            }
        }
        else
        {
            log.info("Triple Store TDB location is " + file.getAbsolutePath());
        }
    }

    /**
     * Constructs a new TDB database from assembler file if necessary
     * or opens a connection to the existing triple store
     * @param assemblerFile specifying the creation of the Jena TDB triple store
     */
    public TDBConnection(File assemblerFile)
    {
        if (assemblerFile == null || !assemblerFile.exists())
        {
            log.error("You are missing the required file " + assemblerFile + " . The dataset can not be instantiated.");
            System.exit(1);
        }
        dataset = TDBFactory.assembleDataset(assemblerFile.getAbsolutePath());
    }

    /**
     * Opens a new READ/WRITE transaction to the database
     */
    public void openTransactionReadWrite()
    {
        dataset.begin(ReadWrite.WRITE);
    }

    /**
     * Ends a transaction and commits (success)
     */
    public void endTransactionCommit()
    {
        dataset.commit();
        dataset.end();
    }

    /**
     * Ends a transaction and aborts (rollback)
     */
    public void endTransactionAbort()
    {
        dataset.abort();
        dataset.end();
    }

    /**
     * Closes the connection to the triple store
     */
    public void close()
    {
        dataset.close();
    }

    /**
     * Gets a named graph with the specified @param modelName from the collection of named graphs in the DataSet.
     * If @param modelName is null the default model is returned
     * @return the Model with the specified name or the default model if null
     */
    public Model getTDBModel(String modelName)
    {
        return getNamedOrDefaultModel(modelName);
    }

    /**
     * Adds the given statements @param stmts to the model identified by @param modelName.
     * If @param modelName is null statements are added to the default model
     */
    public void addStatements(String modelName, List<Statement> stmts)
    {
        Model model = getNamedOrDefaultModel(modelName);
        model.add(stmts);
    }

    /**
     * Selects all the statements from the model identified by @param modelName
     * that match a selector for @param subject @param predicate @param object.
     * If any triple param s,p,o is null it matches anything.
     * If @param modelName is null the default model is queried.
     * @return a list of statements matching the parameters
     */
    public List<Statement> getStatements(String modelName, String subject, String predicate, String object)
    {
        List<Statement> results = new ArrayList<Statement>();

        Model model = getNamedOrDefaultModel(modelName);

        Selector selector = new SimpleSelector(
            (subject != null) ? model.createResource(subject) : (Resource) null,
            (predicate != null) ? model.createProperty(predicate) : (Property) null,
            (object != null) ? model.createResource(object) : (RDFNode) null);

        StmtIterator it = model.listStatements(selector);
        {
            while (it.hasNext())
            {
                Statement stmt = it.next();
                results.add(stmt);
            }
        }

        return results;
    }

    /**
     * Removes a list of statements @param stmts from the model identified by @param modelName.
     * If @param modelName is null statements are deleted from the default model.
     * If the statements do not exist in the model nothing happens.
     */
    public void removeStatements(String modelName, List<Statement> stmts)
    {
        Model model = getNamedOrDefaultModel(modelName);
        model.remove(stmts);
    }

    /**
     * Removes a list of statements @param stmts from the model identified by @param modelName.
     * If @param modelName is null statements are deleted from the default model.
     * If the statements do not exist in the model nothing happens.
     */
    public void removeStatement(String modelName, Statement stmt)
    {
        Model model = getNamedOrDefaultModel(modelName);
        model.remove(stmt);
    }

    /**
     * @return the model corresponding to the named graph identified by @param modelName 
     * or the default graph from the DataSet
     */
    private Model getNamedOrDefaultModel(String modelName)
    {
        if (modelName != null)
        {
            return dataset.getNamedModel(modelName);
        }
        else
        {
            return dataset.getDefaultModel();
        }
    }

}
