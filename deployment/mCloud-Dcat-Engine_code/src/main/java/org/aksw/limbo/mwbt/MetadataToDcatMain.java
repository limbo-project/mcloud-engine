package org.aksw.limbo.mwbt;

import java.io.File;
import java.io.IOException;

import org.aksw.limbo.mwbt.dcat.DcatConverter;
import org.aksw.limbo.mwbt.util.ParameterHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.JCommander;


/**
 * @author ivierle
 * entrypoint class
 */
public class MetadataToDcatMain
{
    private static final Logger log = LoggerFactory.getLogger(MetadataToDcatMain.class);

    protected final static String USAGE =
        "\njava -jar mCloud-DCAT-Engine-version.jar -i <path/to/input> -s <true/false> [-o <path/to/store/output>] [ -a <path/to/dcat/directory>]\n";

    public static void main(String[] args)
    {
        long startTime = System.currentTimeMillis();

        File outputFile = null;
        File inputDirectory = null;
        File dcatDirectory = null;
        File tdbLocation = null;

        ParameterHelper parameters = new ParameterHelper();
        JCommander jc = JCommander.newBuilder().addObject(parameters).build();
        jc.parse(args);

        if (parameters.isHelp())
        {
            log.info(USAGE);
            jc.usage();
            return;
        }

        try
        {
            outputFile = parameters.validateAndGetOutputFile();
            inputDirectory = parameters.getInputDirectory();
            dcatDirectory = parameters.getDcatDirectory();
            tdbLocation = parameters.getTDBAssemblerFile();

            DcatConverter converter = new DcatConverter(parameters.isStoreToTDB(), tdbLocation);
            converter.triggerCatalogCreation(inputDirectory, outputFile, dcatDirectory);
        }
        catch (RuntimeException | IOException e)
        {
            if (e instanceof IOException)
            {
                log.error("[FileCreationError] Required output files could not be created.", e);
            }
            else
            {
                log.error("[LogicalError] An unexpected runtime error occured. Please contact the developer an provide this exception.", e);
            }

            //clean up empty or error prone output file
            if (outputFile != null)
            {
                String path = outputFile.getAbsolutePath();
                if (outputFile.delete())
                {
                    log.debug("Deleted output file on error: " + path);
                }
            }
            System.exit(1);
        }
        //log the runtime
        long runTime = System.currentTimeMillis() - startTime;
        System.out.println("Runtime:\t " + runTime + " milliseconds");
    }

}
