package org.aksw.limbo.mwbt.util.jena;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author ivierle
 * Helperclass that handles reading and writing of graphs via the JENA Model API
 */
public class JenaFileIOHandler
{
    private static final Logger log = LoggerFactory.getLogger(JenaFileIOHandler.class);

    /**
     * Since data output files from SQUIRREL do not have a file extension 
     * we need to tell Jena which file type to expect to prevent errors
     */
    private static final Lang assumedLanguage = Lang.TURTLE;

    /**
     * Takes an arbitrary number of resource files (must be Jena conform {@link https://jena.apache.org/documentation/io/}) 
     * and combines them into one model.  
     * The language is to be assumed by the user as for now file extensions are missing
     * @param pathToResources directory in which the resources are stored
     * @return the model containing all resources
     */
    public Model readRDFFilesIntoModel(File[] resourceFiles)
    {
        Model model = null;
        List<File> resources = new ArrayList<File>();
        File baseResource = null;

        for (File file : resourceFiles)
        {
            if (file.isFile())
            {
                resources.add(file);
            }
        }

        if (!resources.isEmpty())
        {
            baseResource = resources.remove(0);
        }

        if (baseResource != null)
        {
            model = RDFDataMgr.loadModel(baseResource.getPath(), assumedLanguage);
            for (File file : resources)
            {
                //checks for conformity with asserted language of files 
                model.read(file.getPath(), assumedLanguage.getName());
            }
        }
        return model;
    }

    /**
     * Writes a given RDF graph in form of a Jena @param model to the specified @param outputFile 
     * using the user defines language @param lang (for now Lang.TTL)
     */
    public void writeModelToFile(Model model, File outputFile, Lang lang)
    {
        log.info("Writing catalog to " + outputFile.getAbsolutePath());

        PrintWriter out = null;
        try
        {
            out = new PrintWriter(outputFile);
            model.write(out, lang.getName());
        }
        catch (FileNotFoundException e)
        {
            log.error("Error writing to file " + outputFile.getAbsolutePath(), e);
        }
        finally
        {
            if (out != null)
            {
                out.flush();
                out.close();
            }
        }
    }
}
