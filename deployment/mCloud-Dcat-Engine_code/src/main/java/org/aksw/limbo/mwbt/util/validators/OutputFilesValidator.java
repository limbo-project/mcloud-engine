package org.aksw.limbo.mwbt.util.validators;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

/**
 * @author ivierle
 * Validatorclass to verify the output parameters
 */
public class OutputFilesValidator implements IParameterValidator
{
    private static final Logger log = LoggerFactory.getLogger(OutputFilesValidator.class);

    /**
     * {@inheritDoc}
     * Checks if the given output directory path exists, if not throws {@link ParameterException} and terminates the program.
     */
    @Override
    public void validate(String name, String value) throws ParameterException
    {
        File file = new File(value);
        if (!file.exists())
        {
            log.error("[ValidationError] The output directory does not exist. Please check if the given path is correct or use --help for instructions.",
                new ParameterException("Parameter " + name + " does not point to a valid directory"));
            System.exit(1);
        }
    }
}
