package org.aksw.limbo.mwbt.util.validators;

import org.aksw.limbo.mwbt.util.validators.OutputFilesValidator;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;


public class OutputFilesValidatorTest
{
    OutputFilesValidator validator;

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Before
    public void setUp()
    {
        validator = new OutputFilesValidator();
    }

    @Test
    public void testValidateDirectoryDoesNotExist()
    {
        exit.expectSystemExitWithStatus(1);
        validator.validate("-o", "path/does/not/exist");
    }

    @Test
    public void testValidate()
    {
        String validDirectory = "src/test/resources/testScenarios/validators/scenario2";
        validator.validate("-o", validDirectory);
    }

}
