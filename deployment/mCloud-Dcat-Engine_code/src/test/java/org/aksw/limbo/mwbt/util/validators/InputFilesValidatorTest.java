package org.aksw.limbo.mwbt.util.validators;

import org.aksw.limbo.mwbt.util.validators.InputFilesValidator;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;


public class InputFilesValidatorTest
{
    InputFilesValidator validator;

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Before
    public void setUp()
    {
        validator = new InputFilesValidator();
    }

    @Test
    public void testValidateInputDirectoryDoesNotExist()
    {
        exit.expectSystemExitWithStatus(1);
        validator.validate("-i", "path/to/input/does/not/exist");
    }

    @Test
    public void testValidateInputDirectoryIsEmpty()
    {
        String emptyDirectory = "src/test/resources/testScenarios/validators/scenario1/emptyInput";
        exit.expectSystemExitWithStatus(1);
        validator.validate("-i", emptyDirectory);
    }

    @Test
    public void testValidate()
    {
        String validDirectory = "src/test/resources/testScenarios/validators/scenario1/successValidationInput";
        validator.validate("-i", validDirectory);
    }

}
