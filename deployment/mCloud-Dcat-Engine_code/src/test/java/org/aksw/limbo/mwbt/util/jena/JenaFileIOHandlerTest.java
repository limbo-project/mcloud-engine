package org.aksw.limbo.mwbt.util.jena;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;

import org.aksw.limbo.mwbt.util.jena.JenaFileIOHandler;
import org.apache.jena.ext.com.google.common.collect.Iterators;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.riot.Lang;
import org.junit.Before;
import org.junit.Test;


public class JenaFileIOHandlerTest
{
    JenaFileIOHandler handler;
    Model testModel;

    @Before
    public void setUp()
    {
        handler = new JenaFileIOHandler();
        testModel = ModelFactory.createDefaultModel();
    }

    @Test
    public void testReadRDFFilesIntoModelNull()
    {
        File empty = new File("src/test/resources/testScenarios/util/jena/empty");
        assertNull(handler.readRDFFilesIntoModel(empty.listFiles()));

        File onlyDirectories = new File("src/test/resources/testScenarios/util/jena");
        assertNull(handler.readRDFFilesIntoModel(onlyDirectories.listFiles()));
    }

    @Test
    public void testWriteModelToFileANDtestReadRDFFilesIntoModel()
    {
        //testWriteModelToFile
        File output = new File("src/test/resources/testScenarios/util/jena/io/testWrittenModel.ttl");

        Property p = ResourceFactory.createProperty("http://test.predicate.com");
        Resource resource = testModel.createResource("http://test.subject.com");
        resource.addProperty(p, testModel.createResource("http://test.object.com"));

        handler.writeModelToFile(testModel, output, Lang.TTL);

        //assert that model was correctly written: testReadRDFFilesIntoModel
        Model model = handler.readRDFFilesIntoModel(output.getParentFile().listFiles());
        assertNotNull(model);
        assertEquals(1, Iterators.size(model.listStatements()));

        Statement stmt = model.listStatements().next();
        assertEquals("http://test.subject.com", stmt.getSubject().getURI());
        assertEquals("http://test.predicate.com", stmt.getPredicate().getURI());
        assertEquals("http://test.object.com", stmt.getResource().getURI());

    }
}
