package org.aksw.limbo.mwbt.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.aksw.limbo.mwbt.util.ParameterHelper;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

import com.beust.jcommander.JCommander;


public class ParameterHelperTest
{
    JCommander jc;
    ParameterHelper helper;

    String input;
    String output;
    boolean store;

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Before
    public void setUp()
    {
        helper = new ParameterHelper();
        jc = JCommander.newBuilder().addObject(helper).build();

        input = "src/test/resources/testScenarios/util/input";
        output = "src/test/resources/testScenarios/util/output";
        store = false;
    }

    @After
    public void tearDown() throws IOException
    {
        FileUtils.cleanDirectory(new File(input + "/catalogs"));
        FileUtils.cleanDirectory(new File(output));
    }

    @Test
    public void testValidateAndGetOutputFileDefault() throws IOException
    {
        String[] args = {"-i", input, "-s", "false"};
        jc.parse(args);

        File createdFile = helper.validateAndGetOutputFile();
        assertEquals("src/test/resources/testScenarios/util/input/catalogs", createdFile.getParent());
        assertTrue(createdFile.getName().matches("dcat-mCloud-metadata-\\[.*\\]\\.ttl"));
    }

    @Test
    public void testValidateAndGetOutputFile() throws IOException
    {
        String[] args = {"-i", input, "-s", "false", "-o", output};
        jc.parse(args);

        File createdFile = helper.validateAndGetOutputFile();
        assertEquals(output, createdFile.getParent());
        assertTrue(createdFile.getName().matches("dcat-mCloud-metadata-\\[.*\\]\\.ttl"));
    }
}
