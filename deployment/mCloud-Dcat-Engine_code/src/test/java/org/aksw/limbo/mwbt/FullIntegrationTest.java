package org.aksw.limbo.mwbt;

import static org.hamcrest.collection.IsArrayWithSize.arrayWithSize;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeThat;
import static org.junit.Assume.assumeTrue;

import java.io.File;

import org.aksw.limbo.mwbt.MetadataToDcatMain;
import java.io.IOException;

import org.aksw.limbo.mwbt.util.jena.JenaFileIOHandler;
import org.apache.commons.io.FileUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.vocabulary.DCAT;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.apache.jena.vocabulary.XSD;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class FullIntegrationTest
{
    String store = "false";
    File baseCatalogDir;
    File updatedCatalogDir;

    JenaFileIOHandler ioHelper;

    String dataSetURI = "http://limbo-mCloudStatisticsEngine.org/metadata#dataset-Basiswarnungen-1145644315";
    String distributionWms1URI = "http://limbo-mCloudStatisticsEngine.org/metadata#distribution-Basiswarnungen-1531780796-WMS";
    String distributionWms2URI = "http://limbo-mCloudStatisticsEngine.org/metadata#distribution-Basiswarnungen--642443913-WMS";
    String distributionWfsURI = "http://limbo-mCloudStatisticsEngine.org/metadata#distribution-Basiswarnungen-1509359930-WFS";
    String distributionDownloadURI = "http://limbo-mCloudStatisticsEngine.org/metadata#distribution-Basiswarnungen-752755487-Dateidownload";
    String publisherUri = "http://limbo-mCloudStatisticsEngine.org/metadata#Deutscher-Wetterdienst-DWD-1357215209";
    String licenseUri = "http://limbo-mCloudStatisticsEngine.org/metadata#Verordnung-zur-Festlegung-der-Nutzungsbestimmungen-für-die-Bereitstellung-von-Geodaten-des-Bundes-GeoNutzV--739825321";

    @Before
    public void setUp() throws Exception
    {
        ioHelper = new JenaFileIOHandler();

        String baseInput = "src/test/resources/testScenarios/base";
        String[] args = {"-i", baseInput, "-s", store};

        MetadataToDcatMain.main(args);

        String catalogLocation = baseInput + "/catalogs";
        baseCatalogDir = new File(catalogLocation);
        assumeTrue(baseCatalogDir.exists());
        assumeThat(baseCatalogDir.listFiles(), arrayWithSize(1));
    }

    @After
    public void tearDown() throws Exception
    {
        FileUtils.deleteDirectory(baseCatalogDir);
        FileUtils.deleteDirectory(updatedCatalogDir);
    }

    ////
    // Test scenarios
    ////

    @Test
    public void testMainScenario1()
    {
        Model baseModel = ioHelper.readRDFFilesIntoModel(baseCatalogDir.listFiles());

        String updateInput = "src/test/resources/testScenarios/scenario1";
        String[] args = {"-i", updateInput, "-s", store, "-a", "src/test/resources/testScenarios/base/catalogs"};

        MetadataToDcatMain.main(args);
        updatedCatalogDir = assertCatalogCreationAndReturnFile(updateInput);

        Model updatedModel = ioHelper.readRDFFilesIntoModel(updatedCatalogDir.listFiles());

        //modifications: only timestamps changed, no change should've been applied
        assertTrue(updatedModel.containsAll(baseModel));
    }

    @Test
    public void testMainScenario2()
    {
        String updateInput = "src/test/resources/testScenarios/scenario2";
        String[] args = {"-i", updateInput, "-s", store, "-a", baseCatalogDir.getPath()};
        MetadataToDcatMain.main(args);

        updatedCatalogDir = assertCatalogCreationAndReturnFile(updateInput);
        Model updatedModel = ioHelper.readRDFFilesIntoModel(updatedCatalogDir.listFiles());

        //basics
        assertCatalog(updatedModel);
        assertAccessTypes(updatedModel);
        assertPublisherLicsense(updatedModel);

        //modifications: update a distribution
        Resource distWFS = updatedModel.getResource(distributionWfsURI);
        assertTrue(distWFS.hasProperty(RDF.type, DCAT.Distribution));

        assertTrue(distWFS.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(distWFS.hasProperty(DCTerms.modified, updatedModel.createTypedLiteral("2018-06-30T18:21:00", XSD.dateTime.getURI())));
        assertTrue(distWFS.hasProperty(DCTerms.title, "UPDATED WFS distribution of dataset http://limbo-mCloudStatisticsEngine.org/metadata#dataset-Basiswarnungen-1145644315"));
        assertTrue(distWFS.hasProperty(DCTerms.temporal, updatedModel.createTypedLiteral("UPDATED Echtzeit", DCTerms.PeriodOfTime.getURI())));
        assertTrue(distWFS.hasProperty(updatedModel.createProperty("http://limbo-mCloudStatisticsEngine.org/metadata#accessType"),
            updatedModel.getResource("http://limbo-mCloudStatisticsEngine.org/metadata#WFS")));
        assertTrue(distWFS.hasProperty(DCAT.accessURL, "https://maps.dwd.de/geoserver/dwd/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=dwd:BASISWARNUNGEN"));
        assertTrue(distWFS.hasProperty(DCTerms.license, updatedModel.getResource(licenseUri)));

        Resource distWMS1 = updatedModel.getResource(distributionWms1URI);
        assertTrue(distWMS1.hasProperty(RDF.type, DCAT.Distribution));
        assertTrue(distWMS1.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(distWMS1.hasProperty(DCTerms.temporal, updatedModel.createTypedLiteral("Echtzeit", DCTerms.PeriodOfTime.getURI())));
        assertTrue(distWMS1.hasProperty(DCTerms.title, "WMS distribution of dataset http://limbo-mCloudStatisticsEngine.org/metadata#dataset-Basiswarnungen-1145644315"));
        assertTrue(distWMS1.hasProperty(updatedModel.createProperty("http://limbo-mCloudStatisticsEngine.org/metadata#accessType"),
            updatedModel.getResource("http://limbo-mCloudStatisticsEngine.org/metadata#WMS")));
        assertTrue(distWMS1.hasProperty(DCAT.accessURL, "https://maps.dwd.de/geoserver/dwd/wms?service=WMS&version=1.1.0&request=GetCapabilities"));
        assertTrue(distWMS1.hasProperty(DCTerms.license, updatedModel.getResource(licenseUri)));
        assertFalse(distWMS1.hasProperty(DCTerms.modified));

        Resource distWMS2 = updatedModel.getResource(distributionWms2URI);
        assertTrue(distWMS2.hasProperty(RDF.type, DCAT.Distribution));
        assertTrue(distWMS2.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(distWMS2.hasProperty(DCTerms.temporal, updatedModel.createTypedLiteral("Echtzeit", DCTerms.PeriodOfTime.getURI())));
        assertTrue(distWMS2.hasProperty(DCTerms.title, "WMS distribution of dataset http://limbo-mCloudStatisticsEngine.org/metadata#dataset-Basiswarnungen-1145644315"));
        assertTrue(distWMS2.hasProperty(updatedModel.createProperty("http://limbo-mCloudStatisticsEngine.org/metadata#accessType"),
            updatedModel.getResource("http://limbo-mCloudStatisticsEngine.org/metadata#WMS")));
        assertTrue(distWMS2.hasProperty(DCAT.accessURL, "https://maps.dwd.de/geoserver/dwd/wms?service=WMS&version=1.1.0&request=GetMap&layers=dwd:BASISWARNUNGEN"));
        assertTrue(distWMS2.hasProperty(DCTerms.license, updatedModel.getResource(licenseUri)));
        assertFalse(distWMS2.hasProperty(DCTerms.modified));

        Resource dataset = updatedModel.getResource(dataSetURI);
        assertTrue(dataset.hasProperty(RDF.type, DCAT.Dataset));
        assertTrue(dataset.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(dataset.hasProperty(DCTerms.modified, updatedModel.createTypedLiteral("2018-06-30T18:21:00", XSD.dateTime.getURI())));

        assertTrue(dataset.hasProperty(DCTerms.title, "Basiswarnungen"));
        assertTrue(dataset.hasProperty(DCTerms.description, "Aktuelle Wetterwarnungen"));
        assertTrue(dataset.hasProperty(DCAT.keyword, updatedModel.createLiteral("Klima und Wetter", "de")));
        assertTrue(dataset.hasProperty(DCAT.landingPage,
            "https://www.mcloud.de/web/guest/suche/-/results/detail/basiswarnungen?_mcloudsearchportlet_backURL=https%3A%2F%2Fwww.mcloud.de%2Fweb%2Fguest%2Fsuche%2F-%2Fresults%2FsearchAction%3F_mcloudsearchportlet_page%3D48%26_mcloudsearchportlet_sort%3Dlatest"));
        assertTrue(dataset.hasProperty(DCTerms.publisher, updatedModel.getResource(publisherUri)));
        assertTrue(dataset.hasProperty(DCAT.distribution, distWMS1));
        assertTrue(dataset.hasProperty(DCAT.distribution, distWMS2));
        assertTrue(dataset.hasProperty(DCAT.distribution, distWFS));
    }

    @Test
    public void testMainScenario3()
    {
        String updateInput = "src/test/resources/testScenarios/scenario3";
        String[] args = {"-i", updateInput, "-s", store, "-a", baseCatalogDir.getPath()};
        MetadataToDcatMain.main(args);

        updatedCatalogDir = assertCatalogCreationAndReturnFile(updateInput);
        Model updatedModel = ioHelper.readRDFFilesIntoModel(updatedCatalogDir.listFiles());

        //basics
        assertCatalog(updatedModel);
        assertAccessTypes(updatedModel);
        assertPublisherLicsense(updatedModel);

        //modifications: update a distribution
        Resource distWFS = updatedModel.getResource(distributionWfsURI);
        assertTrue(distWFS.hasProperty(RDF.type, DCAT.Distribution));
        assertTrue(distWFS.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(distWFS.hasProperty(DCTerms.modified, updatedModel.createTypedLiteral("2018-06-30T18:21:00", XSD.dateTime.getURI())));
        assertTrue(distWFS.hasProperty(DCTerms.title, "UPDATED WFS distribution of dataset http://limbo-mCloudStatisticsEngine.org/metadata#dataset-Basiswarnungen-1145644315"));
        assertTrue(distWFS.hasProperty(DCTerms.temporal, updatedModel.createTypedLiteral("UPDATED Echtzeit", DCTerms.PeriodOfTime.getURI())));
        assertTrue(distWFS.hasProperty(updatedModel.createProperty("http://limbo-mCloudStatisticsEngine.org/metadata#accessType"),
            updatedModel.getResource("http://limbo-mCloudStatisticsEngine.org/metadata#WFS")));
        assertTrue(distWFS.hasProperty(DCAT.accessURL, "https://maps.dwd.de/geoserver/dwd/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=dwd:BASISWARNUNGEN"));
        assertTrue(distWFS.hasProperty(DCTerms.license, updatedModel.getResource(licenseUri)));

        Resource distWMS1 = updatedModel.getResource(distributionWms1URI);
        assertTrue(distWMS1.hasProperty(RDF.type, DCAT.Distribution));
        assertTrue(distWMS1.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(distWMS1.hasProperty(DCTerms.modified, updatedModel.createTypedLiteral("2017-07-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(distWMS1.hasProperty(DCTerms.temporal, updatedModel.createTypedLiteral("Echtzeit", DCTerms.PeriodOfTime.getURI())));
        assertTrue(distWMS1.hasProperty(DCTerms.title, "UPDATED WMS distribution of dataset http://limbo-mCloudStatisticsEngine.org/metadata#dataset-Basiswarnungen-1145644315"));
        assertTrue(distWMS1.hasProperty(updatedModel.createProperty("http://limbo-mCloudStatisticsEngine.org/metadata#accessType"),
            updatedModel.getResource("http://limbo-mCloudStatisticsEngine.org/metadata#WMS")));
        assertTrue(distWMS1.hasProperty(DCAT.accessURL, "https://maps.dwd.de/geoserver/dwd/wms?service=WMS&version=1.1.0&request=GetCapabilities"));
        assertTrue(distWMS1.hasProperty(DCTerms.license, updatedModel.getResource(licenseUri)));

        Resource distWMS2 = updatedModel.getResource(distributionWms2URI);
        assertTrue(distWMS2.hasProperty(RDF.type, DCAT.Distribution));
        assertTrue(distWMS2.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(distWMS2.hasProperty(DCTerms.temporal, updatedModel.createTypedLiteral("Echtzeit", DCTerms.PeriodOfTime.getURI())));
        assertTrue(distWMS2.hasProperty(DCTerms.title, "WMS distribution of dataset http://limbo-mCloudStatisticsEngine.org/metadata#dataset-Basiswarnungen-1145644315"));
        assertTrue(distWMS2.hasProperty(updatedModel.createProperty("http://limbo-mCloudStatisticsEngine.org/metadata#accessType"),
            updatedModel.getResource("http://limbo-mCloudStatisticsEngine.org/metadata#WMS")));
        assertTrue(distWMS2.hasProperty(DCAT.accessURL, "https://maps.dwd.de/geoserver/dwd/wms?service=WMS&version=1.1.0&request=GetMap&layers=dwd:BASISWARNUNGEN"));
        assertTrue(distWMS2.hasProperty(DCTerms.license, updatedModel.getResource(licenseUri)));
        assertFalse(distWMS2.hasProperty(DCTerms.modified));

        Resource dataset = updatedModel.getResource(dataSetURI);
        assertTrue(dataset.hasProperty(RDF.type, DCAT.Dataset));
        assertTrue(dataset.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(dataset.hasProperty(DCTerms.modified, updatedModel.createTypedLiteral("2018-06-30T18:21:00", XSD.dateTime.getURI())));
        assertTrue(dataset.hasProperty(DCTerms.title, "Basiswarnungen"));
        assertTrue(dataset.hasProperty(DCTerms.description, "Aktuelle Wetterwarnungen"));
        assertTrue(dataset.hasProperty(DCAT.keyword, updatedModel.createLiteral("Klima und Wetter", "de")));
        assertTrue(dataset.hasProperty(DCAT.landingPage,
            "https://www.mcloud.de/web/guest/suche/-/results/detail/basiswarnungen?_mcloudsearchportlet_backURL=https%3A%2F%2Fwww.mcloud.de%2Fweb%2Fguest%2Fsuche%2F-%2Fresults%2FsearchAction%3F_mcloudsearchportlet_page%3D48%26_mcloudsearchportlet_sort%3Dlatest"));
        assertTrue(dataset.hasProperty(DCTerms.publisher, updatedModel.getResource(publisherUri)));
        assertTrue(dataset.hasProperty(DCAT.distribution, distWMS1));
        assertTrue(dataset.hasProperty(DCAT.distribution, distWMS2));
        assertTrue(dataset.hasProperty(DCAT.distribution, distWFS));
    }

    @Test
    public void testMainScenario4()
    {
        String updateInput = "src/test/resources/testScenarios/scenario4";
        String[] args = {"-i", updateInput, "-s", store, "-a", baseCatalogDir.getPath()};
        MetadataToDcatMain.main(args);

        updatedCatalogDir = assertCatalogCreationAndReturnFile(updateInput);
        Model updatedModel = ioHelper.readRDFFilesIntoModel(updatedCatalogDir.listFiles());

        //basics
        assertCatalog(updatedModel);
        assertAccessTypes(updatedModel);
        assertPublisherLicsense(updatedModel);

        //modifications: add a new distribution
        Resource newType = updatedModel.getResource("http://limbo-mCloudStatisticsEngine.org/metadata#Dateidownload");
        assertTrue(newType.hasProperty(RDFS.label, "Dateidownload"));

        Resource distDownload = updatedModel.getResource(distributionDownloadURI);
        assertTrue(distDownload.hasProperty(RDF.type, DCAT.Distribution));
        assertTrue(distDownload.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2018-06-30T09:32:44", XSD.dateTime.getURI())));
        assertTrue(distDownload.hasProperty(DCTerms.temporal, updatedModel.createTypedLiteral("wow so new, very update", DCTerms.PeriodOfTime.getURI())));
        assertTrue(distDownload.hasProperty(DCTerms.title, "Test add a new distribution"));
        assertTrue(distDownload.hasProperty(updatedModel.createProperty("http://limbo-mCloudStatisticsEngine.org/metadata#accessType"),
            updatedModel.getResource("http://limbo-mCloudStatisticsEngine.org/metadata#Dateidownload")));
        assertTrue(distDownload.hasProperty(DCAT.downloadURL, "https://maps.dwd.de/geoserver/dwd/Dateidownload"));

        assertTrue(distDownload.hasProperty(DCTerms.license, updatedModel.getResource(licenseUri)));
        assertFalse(distDownload.hasProperty(DCTerms.modified));

        Resource distWFS = updatedModel.getResource(distributionWfsURI);
        assertTrue(distWFS.hasProperty(RDF.type, DCAT.Distribution));
        assertTrue(distWFS.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(distWFS.hasProperty(DCTerms.title, "WFS distribution of dataset http://limbo-mCloudStatisticsEngine.org/metadata#dataset-Basiswarnungen-1145644315"));
        assertTrue(distWFS.hasProperty(DCTerms.temporal, updatedModel.createTypedLiteral("Echtzeit", DCTerms.PeriodOfTime.getURI())));
        assertTrue(distWFS.hasProperty(updatedModel.createProperty("http://limbo-mCloudStatisticsEngine.org/metadata#accessType"),
            updatedModel.getResource("http://limbo-mCloudStatisticsEngine.org/metadata#WFS")));
        assertTrue(distWFS.hasProperty(DCAT.accessURL, "https://maps.dwd.de/geoserver/dwd/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=dwd:BASISWARNUNGEN"));
        assertTrue(distWFS.hasProperty(DCTerms.license, updatedModel.getResource(licenseUri)));
        assertFalse(distWFS.hasProperty(DCTerms.modified));

        Resource distWMS1 = updatedModel.getResource(distributionWms1URI);
        assertTrue(distWMS1.hasProperty(RDF.type, DCAT.Distribution));
        assertTrue(distWMS1.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(distWMS1.hasProperty(DCTerms.temporal, updatedModel.createTypedLiteral("Echtzeit", DCTerms.PeriodOfTime.getURI())));
        assertTrue(distWMS1.hasProperty(DCTerms.title, "WMS distribution of dataset http://limbo-mCloudStatisticsEngine.org/metadata#dataset-Basiswarnungen-1145644315"));
        assertTrue(distWMS1.hasProperty(updatedModel.createProperty("http://limbo-mCloudStatisticsEngine.org/metadata#accessType"),
            updatedModel.getResource("http://limbo-mCloudStatisticsEngine.org/metadata#WMS")));
        assertTrue(distWMS1.hasProperty(DCAT.accessURL, "https://maps.dwd.de/geoserver/dwd/wms?service=WMS&version=1.1.0&request=GetCapabilities"));
        assertTrue(distWMS1.hasProperty(DCTerms.license, updatedModel.getResource(licenseUri)));
        assertFalse(distWMS1.hasProperty(DCTerms.modified));

        Resource distWMS2 = updatedModel.getResource(distributionWms2URI);
        assertTrue(distWMS2.hasProperty(RDF.type, DCAT.Distribution));
        assertTrue(distWMS2.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(distWMS2.hasProperty(DCTerms.temporal, updatedModel.createTypedLiteral("Echtzeit", DCTerms.PeriodOfTime.getURI())));
        assertTrue(distWMS2.hasProperty(DCTerms.title, "WMS distribution of dataset http://limbo-mCloudStatisticsEngine.org/metadata#dataset-Basiswarnungen-1145644315"));
        assertTrue(distWMS2.hasProperty(updatedModel.createProperty("http://limbo-mCloudStatisticsEngine.org/metadata#accessType"),
            updatedModel.getResource("http://limbo-mCloudStatisticsEngine.org/metadata#WMS")));
        assertTrue(distWMS2.hasProperty(DCAT.accessURL, "https://maps.dwd.de/geoserver/dwd/wms?service=WMS&version=1.1.0&request=GetMap&layers=dwd:BASISWARNUNGEN"));
        assertTrue(distWMS2.hasProperty(DCTerms.license, updatedModel.getResource(licenseUri)));
        assertFalse(distWMS2.hasProperty(DCTerms.modified));

        Resource dataset = updatedModel.getResource(dataSetURI);
        assertTrue(dataset.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(dataset.hasProperty(DCTerms.modified, updatedModel.createTypedLiteral("2018-06-30T09:32:44", XSD.dateTime.getURI())));
        assertTrue(dataset.hasProperty(RDF.type, DCAT.Dataset));
        assertTrue(dataset.hasProperty(DCTerms.title, "Basiswarnungen"));
        assertTrue(dataset.hasProperty(DCTerms.description, "Aktuelle Wetterwarnungen"));
        assertTrue(dataset.hasProperty(DCAT.keyword, updatedModel.createLiteral("Klima und Wetter", "de")));
        assertTrue(dataset.hasProperty(DCAT.landingPage,
            "https://www.mcloud.de/web/guest/suche/-/results/detail/basiswarnungen?_mcloudsearchportlet_backURL=https%3A%2F%2Fwww.mcloud.de%2Fweb%2Fguest%2Fsuche%2F-%2Fresults%2FsearchAction%3F_mcloudsearchportlet_page%3D48%26_mcloudsearchportlet_sort%3Dlatest"));
        assertTrue(dataset.hasProperty(DCTerms.publisher, updatedModel.getResource(publisherUri)));
        assertTrue(dataset.hasProperty(DCAT.distribution, distWMS1));
        assertTrue(dataset.hasProperty(DCAT.distribution, distWMS2));
        assertTrue(dataset.hasProperty(DCAT.distribution, distWFS));
        assertTrue(dataset.hasProperty(DCAT.distribution, distDownload));
    }

    @Test
    public void testMainScenario5()
    {
        String updateInput = "src/test/resources/testScenarios/scenario5";
        String[] args = {"-i", updateInput, "-s", store, "-a", baseCatalogDir.getPath()};
        MetadataToDcatMain.main(args);

        updatedCatalogDir = assertCatalogCreationAndReturnFile(updateInput);
        Model updatedModel = ioHelper.readRDFFilesIntoModel(updatedCatalogDir.listFiles());

        //basics
        assertCatalog(updatedModel);
        assertPublisherLicsense(updatedModel);

        Resource wms = updatedModel.getResource("http://limbo-mCloudStatisticsEngine.org/metadata#WMS");
        assertTrue(wms.hasProperty(RDFS.label, "WMS"));

        //modifications: remove a distribution
        Resource distWMS1 = updatedModel.getResource(distributionWms1URI);
        assertTrue(distWMS1.hasProperty(RDF.type, DCAT.Distribution));
        assertTrue(distWMS1.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(distWMS1.hasProperty(DCTerms.temporal, updatedModel.createTypedLiteral("Echtzeit", DCTerms.PeriodOfTime.getURI())));
        assertTrue(distWMS1.hasProperty(DCTerms.title, "WMS distribution of dataset http://limbo-mCloudStatisticsEngine.org/metadata#dataset-Basiswarnungen-1145644315"));
        assertTrue(distWMS1.hasProperty(updatedModel.createProperty("http://limbo-mCloudStatisticsEngine.org/metadata#accessType"),
            updatedModel.getResource("http://limbo-mCloudStatisticsEngine.org/metadata#WMS")));
        assertTrue(distWMS1.hasProperty(DCAT.accessURL, "https://maps.dwd.de/geoserver/dwd/wms?service=WMS&version=1.1.0&request=GetCapabilities"));
        assertTrue(distWMS1.hasProperty(DCTerms.license, updatedModel.getResource(licenseUri)));
        assertFalse(distWMS1.hasProperty(DCTerms.modified));

        Resource distWMS2 = updatedModel.getResource(distributionWms2URI);
        assertTrue(distWMS2.hasProperty(RDF.type, DCAT.Distribution));
        assertTrue(distWMS2.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(distWMS2.hasProperty(DCTerms.temporal, updatedModel.createTypedLiteral("Echtzeit", DCTerms.PeriodOfTime.getURI())));
        assertTrue(distWMS2.hasProperty(DCTerms.title, "WMS distribution of dataset http://limbo-mCloudStatisticsEngine.org/metadata#dataset-Basiswarnungen-1145644315"));
        assertTrue(distWMS2.hasProperty(updatedModel.createProperty("http://limbo-mCloudStatisticsEngine.org/metadata#accessType"),
            updatedModel.getResource("http://limbo-mCloudStatisticsEngine.org/metadata#WMS")));
        assertTrue(distWMS2.hasProperty(DCAT.accessURL, "https://maps.dwd.de/geoserver/dwd/wms?service=WMS&version=1.1.0&request=GetMap&layers=dwd:BASISWARNUNGEN"));
        assertTrue(distWMS2.hasProperty(DCTerms.license, updatedModel.getResource(licenseUri)));
        assertFalse(distWMS2.hasProperty(DCTerms.modified));

        Resource dataset = updatedModel.getResource(dataSetURI);
        assertTrue(dataset.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(dataset.hasProperty(DCTerms.modified, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(dataset.hasProperty(RDF.type, DCAT.Dataset));
        assertTrue(dataset.hasProperty(DCTerms.title, "Basiswarnungen"));
        assertTrue(dataset.hasProperty(DCTerms.description, "Aktuelle Wetterwarnungen"));
        assertTrue(dataset.hasProperty(DCAT.keyword, updatedModel.createLiteral("Klima und Wetter", "de")));
        assertTrue(dataset.hasProperty(DCAT.landingPage,
            "https://www.mcloud.de/web/guest/suche/-/results/detail/basiswarnungen?_mcloudsearchportlet_backURL=https%3A%2F%2Fwww.mcloud.de%2Fweb%2Fguest%2Fsuche%2F-%2Fresults%2FsearchAction%3F_mcloudsearchportlet_page%3D48%26_mcloudsearchportlet_sort%3Dlatest"));
        assertTrue(dataset.hasProperty(DCTerms.publisher, updatedModel.getResource(publisherUri)));
        assertTrue(dataset.hasProperty(DCAT.distribution, distWMS1));
        assertTrue(dataset.hasProperty(DCAT.distribution, distWMS2));

        //assert successful removal
        Resource removed = updatedModel.getResource(distributionWfsURI);
        assertFalse(dataset.hasProperty(DCAT.distribution, removed));
        assertFalse(updatedModel.contains(removed, null));
        assertFalse(updatedModel.contains(updatedModel.getResource("http://limbo-mCloudStatisticsEngine.org/metadata#WFS"), RDFS.label));
    }

    @Test
    public void testMainScenario6()
    {
        String updateInput = "src/test/resources/testScenarios/scenario6";
        String[] args = {"-i", updateInput, "-s", store, "-a", baseCatalogDir.getPath()};
        MetadataToDcatMain.main(args);

        updatedCatalogDir = assertCatalogCreationAndReturnFile(updateInput);
        Model updatedModel = ioHelper.readRDFFilesIntoModel(updatedCatalogDir.listFiles());

        //basics
        assertCatalog(updatedModel);
        assertAccessTypes(updatedModel);

        Resource publisher = updatedModel.getResource(publisherUri);
        assertTrue(publisher.hasProperty(RDF.type, DCTerms.Agent));
        assertTrue(publisher.hasProperty(RDFS.label, "Deutscher Wetterdienst (DWD)"));
        assertTrue(publisher.hasProperty(DCTerms.source, "http://www.dwd.de"));

        //modification: updated license
        Resource license = updatedModel.getResource(licenseUri);
        assertTrue(license.hasProperty(RDF.type, DCTerms.LicenseDocument));
        assertTrue(license.hasProperty(RDFS.label, "aktualisiert (GeoNutzV)"));
        assertTrue(license.hasProperty(DCTerms.source, "http://www.gesetze-im-internet.de/testUpdate"));
        assertFalse(license.hasProperty(RDFS.label, "Verordnung zur Festlegung der Nutzungsbestimmungen für die Bereitstellung von Geodaten des Bundes (GeoNutzV)"));
        assertFalse(license.hasProperty(DCTerms.source, "http://www.gesetze-im-internet.de/geonutzv/index.html"));

        Resource distWFS = updatedModel.getResource(distributionWfsURI);
        assertTrue(distWFS.hasProperty(RDF.type, DCAT.Distribution));
        assertTrue(distWFS.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(distWFS.hasProperty(DCTerms.title, "WFS distribution of dataset http://limbo-mCloudStatisticsEngine.org/metadata#dataset-Basiswarnungen-1145644315"));
        assertTrue(distWFS.hasProperty(DCTerms.temporal, updatedModel.createTypedLiteral("Echtzeit", DCTerms.PeriodOfTime.getURI())));
        assertTrue(distWFS.hasProperty(updatedModel.createProperty("http://limbo-mCloudStatisticsEngine.org/metadata#accessType"),
            updatedModel.getResource("http://limbo-mCloudStatisticsEngine.org/metadata#WFS")));
        assertTrue(distWFS.hasProperty(DCAT.accessURL, "https://maps.dwd.de/geoserver/dwd/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=dwd:BASISWARNUNGEN"));
        assertTrue(distWFS.hasProperty(DCTerms.license, updatedModel.getResource(licenseUri)));

        Resource distWMS1 = updatedModel.getResource(distributionWms1URI);
        assertTrue(distWMS1.hasProperty(RDF.type, DCAT.Distribution));
        assertTrue(distWMS1.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(distWMS1.hasProperty(DCTerms.temporal, updatedModel.createTypedLiteral("Echtzeit", DCTerms.PeriodOfTime.getURI())));
        assertTrue(distWMS1.hasProperty(DCTerms.title, "WMS distribution of dataset http://limbo-mCloudStatisticsEngine.org/metadata#dataset-Basiswarnungen-1145644315"));
        assertTrue(distWMS1.hasProperty(updatedModel.createProperty("http://limbo-mCloudStatisticsEngine.org/metadata#accessType"),
            updatedModel.getResource("http://limbo-mCloudStatisticsEngine.org/metadata#WMS")));
        assertTrue(distWMS1.hasProperty(DCAT.accessURL, "https://maps.dwd.de/geoserver/dwd/wms?service=WMS&version=1.1.0&request=GetCapabilities"));
        assertTrue(distWMS1.hasProperty(DCTerms.license, updatedModel.getResource(licenseUri)));

        Resource distWMS2 = updatedModel.getResource(distributionWms2URI);
        assertTrue(distWMS2.hasProperty(RDF.type, DCAT.Distribution));
        assertTrue(distWMS2.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(distWMS2.hasProperty(DCTerms.temporal, updatedModel.createTypedLiteral("Echtzeit", DCTerms.PeriodOfTime.getURI())));
        assertTrue(distWMS2.hasProperty(DCTerms.title, "WMS distribution of dataset http://limbo-mCloudStatisticsEngine.org/metadata#dataset-Basiswarnungen-1145644315"));
        assertTrue(distWMS2.hasProperty(updatedModel.createProperty("http://limbo-mCloudStatisticsEngine.org/metadata#accessType"),
            updatedModel.getResource("http://limbo-mCloudStatisticsEngine.org/metadata#WMS")));
        assertTrue(distWMS2.hasProperty(DCAT.accessURL, "https://maps.dwd.de/geoserver/dwd/wms?service=WMS&version=1.1.0&request=GetMap&layers=dwd:BASISWARNUNGEN"));
        assertTrue(distWMS2.hasProperty(DCTerms.license, updatedModel.getResource(licenseUri)));

        assertTrue(distWMS1.hasProperty(DCTerms.modified, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(distWMS2.hasProperty(DCTerms.modified, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(distWFS.hasProperty(DCTerms.modified, updatedModel.createTypedLiteral("2018-07-01T00:00:00", XSD.dateTime.getURI())));

        Resource dataset = updatedModel.getResource(dataSetURI);
        assertTrue(dataset.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(dataset.hasProperty(DCTerms.modified, updatedModel.createTypedLiteral("2018-07-01T00:00:00", XSD.dateTime.getURI())));
        assertTrue(dataset.hasProperty(RDF.type, DCAT.Dataset));
        assertTrue(dataset.hasProperty(DCTerms.title, "Basiswarnungen"));
        assertTrue(dataset.hasProperty(DCTerms.description, "Aktuelle Wetterwarnungen"));
        assertTrue(dataset.hasProperty(DCAT.keyword, updatedModel.createLiteral("Klima und Wetter", "de")));
        assertTrue(dataset.hasProperty(DCAT.landingPage,
            "https://www.mcloud.de/web/guest/suche/-/results/detail/basiswarnungen?_mcloudsearchportlet_backURL=https%3A%2F%2Fwww.mcloud.de%2Fweb%2Fguest%2Fsuche%2F-%2Fresults%2FsearchAction%3F_mcloudsearchportlet_page%3D48%26_mcloudsearchportlet_sort%3Dlatest"));
        assertTrue(dataset.hasProperty(DCTerms.publisher, updatedModel.getResource(publisherUri)));
        assertTrue(dataset.hasProperty(DCAT.distribution, distWMS1));
        assertTrue(dataset.hasProperty(DCAT.distribution, distWMS2));
        assertTrue(dataset.hasProperty(DCAT.distribution, distWFS));
        assertTrue(dataset.hasProperty(DCAT.distribution, distWFS));

    }

    @Test
    public void testMainScenario7()
    {
        String updateInput = "src/test/resources/testScenarios/scenario7";
        String[] args = {"-i", updateInput, "-s", store, "-a", baseCatalogDir.getPath()};
        MetadataToDcatMain.main(args);

        updatedCatalogDir = assertCatalogCreationAndReturnFile(updateInput);
        Model updatedModel = ioHelper.readRDFFilesIntoModel(updatedCatalogDir.listFiles());

        String newPublisherUri = "http://limbo-mCloudStatisticsEngine.org/metadata#New-Publisher-URI";

        //basics
        assertCatalog(updatedModel);
        assertAccessTypes(updatedModel);

        Resource license = updatedModel.getResource(licenseUri);
        assertTrue(license.hasProperty(RDF.type, DCTerms.LicenseDocument));
        assertTrue(license.hasProperty(RDFS.label, "Verordnung zur Festlegung der Nutzungsbestimmungen für die Bereitstellung von Geodaten des Bundes (GeoNutzV)"));
        assertTrue(license.hasProperty(DCTerms.source, "http://www.gesetze-im-internet.de/geonutzv/index.html"));

        //modification: updated publisher -> new resource
        Resource publisher = updatedModel.getResource(newPublisherUri);
        assertTrue(publisher.hasProperty(RDF.type, DCTerms.Agent));
        assertTrue(publisher.hasProperty(RDFS.label, "Create a compelety new publisher and remove the old one"));
        assertTrue(publisher.hasProperty(DCTerms.source, "http://www.newPublisher.com"));

        Resource distWFS = updatedModel.getResource(distributionWfsURI);
        assertTrue(distWFS.hasProperty(RDF.type, DCAT.Distribution));
        assertTrue(distWFS.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(distWFS.hasProperty(DCTerms.title, "WFS distribution of dataset http://limbo-mCloudStatisticsEngine.org/metadata#dataset-Basiswarnungen-1145644315"));
        assertTrue(distWFS.hasProperty(DCTerms.temporal, updatedModel.createTypedLiteral("Echtzeit", DCTerms.PeriodOfTime.getURI())));
        assertTrue(distWFS.hasProperty(updatedModel.createProperty("http://limbo-mCloudStatisticsEngine.org/metadata#accessType"),
            updatedModel.getResource("http://limbo-mCloudStatisticsEngine.org/metadata#WFS")));
        assertTrue(distWFS.hasProperty(DCAT.accessURL, "https://maps.dwd.de/geoserver/dwd/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=dwd:BASISWARNUNGEN"));
        assertTrue(distWFS.hasProperty(DCTerms.license, updatedModel.getResource(licenseUri)));
        assertFalse(distWFS.hasProperty(DCTerms.modified));

        Resource distWMS1 = updatedModel.getResource(distributionWms1URI);
        assertTrue(distWMS1.hasProperty(RDF.type, DCAT.Distribution));
        assertTrue(distWMS1.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(distWMS1.hasProperty(DCTerms.temporal, updatedModel.createTypedLiteral("Echtzeit", DCTerms.PeriodOfTime.getURI())));
        assertTrue(distWMS1.hasProperty(DCTerms.title, "WMS distribution of dataset http://limbo-mCloudStatisticsEngine.org/metadata#dataset-Basiswarnungen-1145644315"));
        assertTrue(distWMS1.hasProperty(updatedModel.createProperty("http://limbo-mCloudStatisticsEngine.org/metadata#accessType"),
            updatedModel.getResource("http://limbo-mCloudStatisticsEngine.org/metadata#WMS")));
        assertTrue(distWMS1.hasProperty(DCAT.accessURL, "https://maps.dwd.de/geoserver/dwd/wms?service=WMS&version=1.1.0&request=GetCapabilities"));
        assertTrue(distWMS1.hasProperty(DCTerms.license, updatedModel.getResource(licenseUri)));
        assertFalse(distWMS1.hasProperty(DCTerms.modified));

        Resource distWMS2 = updatedModel.getResource(distributionWms2URI);
        assertTrue(distWMS2.hasProperty(RDF.type, DCAT.Distribution));
        assertTrue(distWMS2.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(distWMS2.hasProperty(DCTerms.temporal, updatedModel.createTypedLiteral("Echtzeit", DCTerms.PeriodOfTime.getURI())));
        assertTrue(distWMS2.hasProperty(DCTerms.title, "WMS distribution of dataset http://limbo-mCloudStatisticsEngine.org/metadata#dataset-Basiswarnungen-1145644315"));
        assertTrue(distWMS2.hasProperty(updatedModel.createProperty("http://limbo-mCloudStatisticsEngine.org/metadata#accessType"),
            updatedModel.getResource("http://limbo-mCloudStatisticsEngine.org/metadata#WMS")));
        assertTrue(distWMS2.hasProperty(DCAT.accessURL, "https://maps.dwd.de/geoserver/dwd/wms?service=WMS&version=1.1.0&request=GetMap&layers=dwd:BASISWARNUNGEN"));
        assertTrue(distWMS2.hasProperty(DCTerms.license, updatedModel.getResource(licenseUri)));
        assertFalse(distWMS2.hasProperty(DCTerms.modified));

        Resource dataset = updatedModel.getResource(dataSetURI);
        assertTrue(dataset.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(dataset.hasProperty(DCTerms.modified, updatedModel.createTypedLiteral("2019-06-07T00:00:00", XSD.dateTime.getURI())));
        assertTrue(dataset.hasProperty(RDF.type, DCAT.Dataset));
        assertTrue(dataset.hasProperty(DCTerms.title, "Basiswarnungen"));
        assertTrue(dataset.hasProperty(DCTerms.description, "Aktuelle Wetterwarnungen"));
        assertTrue(dataset.hasProperty(DCAT.keyword, updatedModel.createLiteral("Klima und Wetter", "de")));
        assertTrue(dataset.hasProperty(DCAT.landingPage,
            "https://www.mcloud.de/web/guest/suche/-/results/detail/basiswarnungen?_mcloudsearchportlet_backURL=https%3A%2F%2Fwww.mcloud.de%2Fweb%2Fguest%2Fsuche%2F-%2Fresults%2FsearchAction%3F_mcloudsearchportlet_page%3D48%26_mcloudsearchportlet_sort%3Dlatest"));
        assertTrue(dataset.hasProperty(DCTerms.publisher, updatedModel.getResource(newPublisherUri)));
        assertTrue(dataset.hasProperty(DCAT.distribution, distWMS1));
        assertTrue(dataset.hasProperty(DCAT.distribution, distWMS2));
        assertTrue(dataset.hasProperty(DCAT.distribution, distWFS));

        //assert successful removal
        Resource removedPublisher = updatedModel.getResource(publisherUri);
        assertFalse(dataset.hasProperty(DCTerms.publisher, removedPublisher));
        assertFalse(updatedModel.contains(removedPublisher, null));

    }

//    @Test
//    public void testMainScenario8() throws IOException
//    {
//        ioHelper = new JenaFileIOHandler();
//        String baseInput = "src/test/resources/testScenarios/scenario8/base";
//        String[] args1 = {"-i", baseInput, "-s", store};
//        MetadataToDcatMain.main(args1);
//        String catalogLocation = baseInput + "/catalogs";
//        baseCatalogDir = new File(catalogLocation);
//        assumeTrue(baseCatalogDir.exists());
//        assumeThat(baseCatalogDir.listFiles(), arrayWithSize(1));
//
//        String updateInput = "src/test/resources/testScenarios/scenario8";
//        String[] args = {"-i", updateInput, "-s", store, "-a", baseCatalogDir.getPath()};
//        MetadataToDcatMain.main(args);
//        updatedCatalogDir = assertCatalogCreationAndReturnFile(updateInput);
//        Model updatedModel = ioHelper.readRDFFilesIntoModel(updatedCatalogDir.listFiles());
//
//        Resource dataset = updatedModel.getResource(dataSetURI);
//        assertTrue(dataset.hasProperty(RDF.type, DCAT.Dataset));
//        assertTrue(dataset.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
//        assertTrue(dataset.hasProperty(DCTerms.modified, updatedModel.createTypedLiteral("2018-06-07T00:00:00", XSD.dateTime.getURI())));
//        assertTrue(dataset.hasProperty(DCTerms.modified, updatedModel.createTypedLiteral("2019-06-07T00:00:00", XSD.dateTime.getURI())));
//
//        Resource distWMS1 = updatedModel.getResource(distributionWms1URI);
//        assertTrue(distWMS1.hasProperty(RDF.type, DCAT.Distribution));
//        assertTrue(distWMS1.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
//        assertTrue(distWMS1.hasProperty(DCTerms.modified, updatedModel.createTypedLiteral("2018-06-07T00:00:00", XSD.dateTime.getURI())));
//        assertTrue(distWMS1.hasProperty(DCTerms.modified, updatedModel.createTypedLiteral("2019-06-07T00:00:00", XSD.dateTime.getURI())));
//        assertTrue(distWMS1.hasProperty(DCTerms.temporal, updatedModel.createTypedLiteral("Fakezeit", DCTerms.PeriodOfTime.getURI())));
//
//        Resource distWFS = updatedModel.getResource(distributionWfsURI);
//        assertTrue(distWFS.hasProperty(RDF.type, DCAT.Distribution));
//        assertTrue(distWFS.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
//        assertTrue(distWFS.hasProperty(DCTerms.temporal, updatedModel.createTypedLiteral("Echtzeit", DCTerms.PeriodOfTime.getURI())));
//        assertTrue(distWFS.hasProperty(DCTerms.modified, updatedModel.createTypedLiteral("2019-06-07T00:00:00", XSD.dateTime.getURI())));
//
//        Resource distWMS2 = updatedModel.getResource(distributionWms2URI);
//        assertTrue(distWMS2.hasProperty(RDF.type, DCAT.Distribution));
//        assertTrue(distWMS2.hasProperty(DCTerms.issued, updatedModel.createTypedLiteral("2017-06-07T00:00:00", XSD.dateTime.getURI())));
//        assertTrue(distWMS2.hasProperty(DCTerms.modified, updatedModel.createTypedLiteral("2018-06-07T00:00:00", XSD.dateTime.getURI())));
//        assertTrue(distWMS2.hasProperty(DCTerms.temporal, updatedModel.createTypedLiteral("Echtzeit", DCTerms.PeriodOfTime.getURI())));
//        assertFalse(distWMS2.hasProperty(DCTerms.modified, updatedModel.createTypedLiteral("2019-06-07T00:00:00", XSD.dateTime.getURI())));
//
//        assertTrue(dataset.hasProperty(DCAT.distribution, distWMS1));
//        assertTrue(dataset.hasProperty(DCAT.distribution, distWMS2));
//        assertTrue(dataset.hasProperty(DCAT.distribution, distWFS));
//
//        FileUtils.deleteDirectory(baseCatalogDir);
//        FileUtils.deleteDirectory(updatedCatalogDir);
//    }

    private File assertCatalogCreationAndReturnFile(String path)
    {
        String catalogLocation = path + "/catalogs";
        File creation = new File(catalogLocation);
        assertTrue(creation.exists());
        assertThat(creation.listFiles(), arrayWithSize(1));

        return creation;
    }

    private void assertCatalog(Model model)
    {
        Resource limbo = model.getResource("http://limbo-mCloudStatisticsEngine.org/metadata#LimboConsortium");
        assertTrue(limbo.hasProperty(RDF.type, FOAF.Organization));
        assertTrue(limbo.hasProperty(RDFS.label, "The Organization publishing the Dcat metadata catalog about mCloud resources"));

        Resource catalog = model.getResource("http://limbo-mCloudStatisticsEngine.org/metadata#Catalog");
        assertTrue(catalog.hasProperty(RDF.type, DCAT.Catalog));
        assertTrue(catalog.hasProperty(DCTerms.language, "http://id.loc.gov/vocabulary/iso639-1/de"));
        assertTrue(catalog.hasProperty(DCTerms.publisher, limbo));
        assertTrue(catalog.hasProperty(DCTerms.title, "Catalog about resources collected by mCloud"));
        assertTrue(catalog.hasProperty(FOAF.homepage, "https://www.limbo-project.org/"));
        assertTrue(catalog.hasProperty(DCTerms.modified));
    }

    private void assertAccessTypes(Model model)
    {
        Resource wms = model.getResource("http://limbo-mCloudStatisticsEngine.org/metadata#WMS");
        assertTrue(wms.hasProperty(RDFS.label, "WMS"));

        Resource wfs = model.getResource("http://limbo-mCloudStatisticsEngine.org/metadata#WFS");
        assertTrue(wfs.hasProperty(RDFS.label, "WFS"));
    }

    private void assertPublisherLicsense(Model model)
    {
        Resource publisher = model.getResource(publisherUri);
        assertTrue(publisher.hasProperty(RDF.type, DCTerms.Agent));
        assertTrue(publisher.hasProperty(RDFS.label, "Deutscher Wetterdienst (DWD)"));
        assertTrue(publisher.hasProperty(DCTerms.source, "http://www.dwd.de"));

        Resource license = model.getResource(licenseUri);
        assertTrue(license.hasProperty(RDF.type, DCTerms.LicenseDocument));
        assertTrue(license.hasProperty(RDFS.label, "Verordnung zur Festlegung der Nutzungsbestimmungen für die Bereitstellung von Geodaten des Bundes (GeoNutzV)"));
        assertTrue(license.hasProperty(DCTerms.source, "http://www.gesetze-im-internet.de/geonutzv/index.html"));
    }
}
